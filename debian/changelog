pyqi (0.3.2+dfsg-13) unstable; urgency=medium

  * fix_sphinx_htmlhelp.patch: normalize last-update timestamp.
  * 2to3.patch: bump support gating to Python 3.13.
    This gating in setup.py does not achieve much and probably ought to be
    removed, but that will suffice for now. (Closes: #1092521)
  * fix-syntax-warning.patch: fix typo caught by lintian.
  * d/control: add myself to uploaders.

 -- Étienne Mollier <emollier@debian.org>  Wed, 08 Jan 2025 22:09:16 +0100

pyqi (0.3.2+dfsg-12) unstable; urgency=medium

  * Team upload.

  [ Andreas Tille ]
  * Remove Tim Booth <tbooth@ceh.ac.uk> since address is bouncing
    (Thank you for your work on this package, Tim)

  [ Emmanuel Arias ]
  * d/patches/fix-syntax-warning.patch: Add patch to fix SyntaxWarning
    (Closes: #1085822).

 -- Emmanuel Arias <eamanu@debian.org>  Mon, 09 Dec 2024 20:56:31 -0300

pyqi (0.3.2+dfsg-10) unstable; urgency=medium

  * Team upload.
  * 2to3.patch: freshed supported python3 versions to add 3.12.
    (Closes: #1074741)
  * d/lintian-overrides: freshen mismatched override.
  * d/control: declare compliance to standards version 4.7.0.
  * d/control: update homepage to (archived) biocore project.

 -- Étienne Mollier <emollier@debian.org>  Wed, 10 Jul 2024 20:51:29 +0200

pyqi (0.3.2+dfsg-9) unstable; urgency=medium

  * Also allow python3.11 as supported option
    Closes: #1028842
  * Standards-Version: 4.6.2 (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Sat, 14 Jan 2023 16:21:28 +0100

pyqi (0.3.2+dfsg-8) unstable; urgency=medium

  * Team Upload.
  * d/p/2to3.patch: Also allow python3.10 as
    supported option (Closes: #1009462)

 -- Nilesh Patra <nilesh@debian.org>  Sat, 16 Apr 2022 02:00:57 +0530

pyqi (0.3.2+dfsg-7) unstable; urgency=medium

  * Team Upload
  * Change supported python versions (Closes: #975828)
  * Superficial import test
  * Override false positive
  * Version: 4 in d/watch
  * sv:4.5.1, compat:13

 -- Nilesh Patra <npatra974@gmail.com>  Fri, 27 Nov 2020 23:39:07 +0530

pyqi (0.3.2+dfsg-6) unstable; urgency=medium

  * Fix htmlhelp in sphinx
    Closes: #959598

 -- Andreas Tille <tille@debian.org>  Mon, 11 May 2020 09:36:38 +0200

pyqi (0.3.2+dfsg-5) unstable; urgency=medium

  [ Andreas Tille ]
  * Fix clean target, enable Python 3.8 in 2to3 patch
    Closes: #954500
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Nilesh Patra ]
  * Remove redundant cgi import line

 -- Andreas Tille <tille@debian.org>  Mon, 30 Mar 2020 15:04:11 +0200

pyqi (0.3.2+dfsg-4) unstable; urgency=medium

  * Use 2to3 to port to Python3
    Closes: #937512
  * buildsystem=pybuild
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Trim trailing whitespace.

 -- Andreas Tille <tille@debian.org>  Wed, 11 Sep 2019 13:39:34 +0200

pyqi (0.3.2+dfsg-3) unstable; urgency=medium

  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Testsuite: autopkgtest-pkg-python
  * Secure URI in copyright format
  * Drop useless get-orig-source target
  * Build-Depends: s/python-sphinx/python3-sphinx/

 -- Andreas Tille <tille@debian.org>  Mon, 29 Oct 2018 13:14:04 +0100

pyqi (0.3.2+dfsg-2) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * cme fix dpkg-control
  * some JS libraries are now in libjs-sphinxdoc
    Closes: #829189

 -- Andreas Tille <tille@debian.org>  Mon, 11 Jul 2016 13:30:25 +0200

pyqi (0.3.2+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 21 May 2014 22:19:41 +0200

pyqi (0.3.1+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Fixed watch file
  * cme fix dpkg-control
  * Exclude binary chunks from doc creation (and egg-info once we are
    removing useless stuff)
  * Create html documentation and install it

 -- Andreas Tille <tille@debian.org>  Thu, 27 Mar 2014 21:32:39 +0100

pyqi (0.2.0-2) unstable; urgency=low

  * Drop centence with funky unicode from long description.
    Closes: #729199

 -- Andreas Tille <tille@debian.org>  Sat, 09 Nov 2013 10:39:14 +0100

pyqi (0.2.0-1) unstable; urgency=low

  * Initial release for Debian (Closes: #726386)
  * debian/control:
     - taking over into Debian Med team maintenance
     - Vcs fields
     - cme fix dpkg-control
     - drop explicit python dependency which is automatically
       resolved from variable
     - Arch: all
  * debian/copyright: cme fix dpkg-copyright

 -- Andreas Tille <tille@debian.org>  Tue, 15 Oct 2013 09:12:04 +0200

pyqi (0.2.0-0biolinux1) precise; urgency=low

  * Initial release for BL.

 -- Tim Booth <tbooth@ceh.ac.uk>  Tue, 24 Sep 2013 18:45:50 +0100
